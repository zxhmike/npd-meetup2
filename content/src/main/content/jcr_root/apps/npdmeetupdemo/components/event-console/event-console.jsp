<%--

  Event Council component.

  Edit events

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page import="
    java.util.Date,
    java.util.List,
    com.northpointdigital.demo.meetup.ocm.model.Event,
	com.northpointdigital.demo.meetup.ocm.model.Activity,
	com.northpointdigital.demo.meetup.ocm.model.Location,
	com.northpointdigital.demo.meetup.ocm.dao.EventDAO
"%>
<%@page session="false" %>
<%
	// Save the object
	EventDAO controller = (EventDAO)sling.getService(EventDAO.class);
	Location locationIn = new Location("NorthPoint Digital", "130 West 42nd Street, New York, NY 10036");
	Event eventIn = new Event("AEM Meetup", "Building Enterprise Web Applications with Adobe AEM", new Date(), locationIn);
	for (int i = 0; i < 10; i++) {
	    Activity activity = new Activity("Activity " + Integer.toString(i), i*10);
	    eventIn.addActivity(activity);
	}
	controller.addOrUpdateEvent(eventIn);
	
	// Retrieve the object
	// You can retrieve by path
	//Event event= controller.getEvent("/content/npdmeetupdemo/events/hello-world");

	// Or by query
	Event event = controller.findEventByName("AEM Meetup");

	// Location is automatically retrieved thanks to the converter
	Location location = event.getLocation();
%>
<h1><%= event.getName() %></h1>
<p> <%= event.getDescription() %> </p>
<p> <%= location.getName() %> </p>
<p> <%= location.getAddress() %> </p>
<p>
<% 
	// Events are automatically retrieved thanks to the "Collection" annotation 
	List<Activity> activities = event.getActivities();
	for (Activity activity : activities) {
	    %><%= activity.getName() %>: <%= activity.getDuration() %> min<br/><%
	}
%>
</p>