package com.northpointdigital.demo.meetup.ocm.util;

public class Formatter {
    public static String jcrFormat(String text) {
        return text.toLowerCase()
                .replaceAll("[^a-z0-9]", "-")
                .replaceAll("-+", "-")
                .replaceAll("^-", "")
                .replaceAll("-$", "");
    }
}
