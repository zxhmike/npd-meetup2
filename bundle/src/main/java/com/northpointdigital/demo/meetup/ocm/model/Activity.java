package com.northpointdigital.demo.meetup.ocm.model;

import org.apache.jackrabbit.ocm.mapper.impl.annotation.Field;
import org.apache.jackrabbit.ocm.mapper.impl.annotation.Node;

@Node
public class Activity {
    @Field private String name;
    @Field private int duration;

    // Important: default constructor is required by OCM
    public Activity() {}
    public Activity(String name, int duration) {
        this.name = name;
        this.duration = duration;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
}
