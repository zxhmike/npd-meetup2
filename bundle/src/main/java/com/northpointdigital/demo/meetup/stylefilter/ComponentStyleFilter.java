package com.northpointdigital.demo.meetup.stylefilter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.components.IncludeOptions;

@Component
@Service
@Properties({
    // Called when including component
    @Property(name = "sling.filter.scope", value = "COMPONENT"), 
    // Makes it later in chain
    @Property(name = "service.ranking", intValue = -1000)
})
/**
 * 
 * @author mike
 * This is the filter that reads the "npdComponentCss" properties of a node and
 * add CSS when the node is being included.
 *
 */
public class ComponentStyleFilter implements javax.servlet.Filter {

    private static final Logger log = LoggerFactory.getLogger(ComponentStyleFilter.class);
    private static final String NPD_COMPONENT_CSS_PROPERTY = "npdComponentCss";

    public void destroy() {
        log.info(ComponentStyleFilter.class.getName() + " destroyed.");
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {
        process(request, response);
        filterChain.doFilter(request, response);
    }

    private void process(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        SlingHttpServletRequest req = (SlingHttpServletRequest) request;

        Resource resource = req.getResource();
        if (resource == null) {
            return;
        }

        ValueMap properties = resource.adaptTo(ValueMap.class);
        if (properties == null) {
            return;
        }

        String componentCss = properties.get(NPD_COMPONENT_CSS_PROPERTY, "");
        if (!componentCss.isEmpty()) {
            IncludeOptions opt = IncludeOptions.getOptions(req, true);
            Set<String> css = opt.getCssClassNames();
            css.addAll(Arrays.asList(componentCss.split(" ")));
            log.debug("Add component CSS: " + componentCss);
        }
    }

    public void init(FilterConfig arg0) throws ServletException {
        log.info(ComponentStyleFilter.class.getName() + " initialized.");
    }
}