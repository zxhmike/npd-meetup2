package com.northpointdigital.demo.meetup.ocm.model;

/**
 * A class that implements this interface has an explicit JCR path
 */
public interface HasPath {
    String getPath();
}
