package com.northpointdigital.demo.meetup.ocm.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.jackrabbit.ocm.mapper.impl.annotation.Bean;
import org.apache.jackrabbit.ocm.mapper.impl.annotation.Collection;
import org.apache.jackrabbit.ocm.mapper.impl.annotation.Field;
import org.apache.jackrabbit.ocm.mapper.impl.annotation.Node;

import com.northpointdigital.demo.meetup.ocm.impl.beanconverter.ReferencePathBeanConverterImpl;
import com.northpointdigital.demo.meetup.ocm.util.Formatter;

@Node
public class Event implements HasPath {
    private static final String ROOT_PATH = "/content/npdmeetupdemo/events";
    @Field(path=true) private String path;
    @Field private String name;
    @Field private String description;
    @Field private Date date;
    @Bean(converter=ReferencePathBeanConverterImpl.class) Location location;
    @Field String locationPath;
    @Collection List<Activity> activities;
    
    public Location getLocation() {
        return location;
    }
    public void setLocation(Location location) {
        this.location = location;
        this.locationPath = location.getPath();
    }
    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    // Important: default constructor is required by OCM
    public Event() {}
    public Event(String name, String description, Date date, Location location) {
        this.name = name;
        this.description = description;
        this.date = date;
        activities = new ArrayList<Activity>();
        setLocation(location);
        this.path = ROOT_PATH + "/" + Formatter.jcrFormat(name);
    }
    
    public String getLocationPath() {
        return locationPath;
    }
    public void setLocationPath(String locationPath) {
        this.locationPath = locationPath;
    }
    public static String getRootPath() {
        return ROOT_PATH;
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public List<Activity> getActivities() {
        return activities;
    }
    public void addActivity(Activity activitie) {
        this.activities.add(activitie);
    } 
}
