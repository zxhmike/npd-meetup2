package com.northpointdigital.demo.meetup.designfilter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Designer;

@Component
@Service
@Properties({
    // Called when including component
    @Property(name = "sling.filter.scope", value = "REQUEST"), 
    // Makes it later in chain
    @Property(name = "service.ranking", intValue = -999)
})
/**
 * 
 * @author mike
 * This filter reads a cookie "npdFromSite" to override 
 * the "currentDesign" object
 *
 */
public class CurrentDesignFilter implements javax.servlet.Filter {

    private static final Logger log = LoggerFactory.getLogger(CurrentDesignFilter.class);
    private static final String CQ_DEFAULT_DESIGN_PATH = "/etc/designs/default";
    private static final String DEFAULT_DESIGN = "default";
    private static final String NPD_FROM_SITE_COOKIE = "npdFromSite";
    private static final String NPD_DESIGN_NAMESPACE = "npdmeetupdemo";
    private static final String REQUEST_PREFIX = 
        com.day.cq.wcm.tags.DefineObjectsTag.class.getName() + ":design:";
    
    public void destroy() {
        log.info(CurrentDesignFilter.class.getName() + " destroyed.");
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {
        SlingHttpServletRequest slingRequest = (SlingHttpServletRequest)request;
        
        // Skip EDIT mode
        if (WCMMode.fromRequest(slingRequest) != WCMMode.EDIT) {
            ResourceResolver resourceResolver = slingRequest.getResourceResolver();
            Page currentPage = (Page)slingRequest.getResource().adaptTo(Page.class);
    
            Cookie[] cookies = slingRequest.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(NPD_FROM_SITE_COOKIE)) {
                        String fromSite = cookie.getValue();
                        String designId = NPD_DESIGN_NAMESPACE + "/" + fromSite;
                        Designer designer = (Designer)resourceResolver.adaptTo(Designer.class);
                        String currentDesignKey = REQUEST_PREFIX + currentPage.getPath();
                        Design newCurrentDesign = designer.getDesign(designId);
                        // CQ returns the default design, override with our default design.
                        if (newCurrentDesign.getPath().equals(CQ_DEFAULT_DESIGN_PATH)) {
                            designId = NPD_DESIGN_NAMESPACE + "/" + DEFAULT_DESIGN;
                            newCurrentDesign = designer.getDesign(designId);
                        }
                        slingRequest.setAttribute(currentDesignKey, newCurrentDesign);
                    }
                }
            }
        }
        filterChain.doFilter(slingRequest, response);
    }

    public void init(FilterConfig arg0) throws ServletException {
        log.info(CurrentDesignFilter.class.getName() + " initialized.");
    }
}