package com.northpointdigital.demo.meetup.ocm.model;

import org.apache.jackrabbit.ocm.mapper.impl.annotation.Field;
import org.apache.jackrabbit.ocm.mapper.impl.annotation.Node;

import com.northpointdigital.demo.meetup.ocm.util.Formatter;

@Node
public class Location implements HasPath {
    public static final String ROOT_PATH = "/content/npdmeetupdemo/locations";
    @Field(path=true) private String path;
    @Field private String name;
    @Field private String address;
    
    public Location() {}
    public Location(String name, String address) {
        this.name = name;
        this.address = address;
        this.path = ROOT_PATH + "/" + Formatter.jcrFormat(name);
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}
