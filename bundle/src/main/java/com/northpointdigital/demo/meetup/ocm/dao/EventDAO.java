package com.northpointdigital.demo.meetup.ocm.dao;

import javax.jcr.RepositoryException;

import com.northpointdigital.demo.meetup.ocm.model.Event;

public interface EventDAO {
    void addOrUpdateEvent(Event event) throws RepositoryException;
    void removeEvent(Event event) throws RepositoryException;
    Event getEvent(String path) throws RepositoryException;
    Event findEventByName(String name) throws RepositoryException;
}
