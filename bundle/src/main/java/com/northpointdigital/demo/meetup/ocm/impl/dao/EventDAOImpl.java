package com.northpointdigital.demo.meetup.ocm.impl.dao;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.ocm.manager.ObjectContentManager;
import org.apache.jackrabbit.ocm.manager.impl.ObjectContentManagerImpl;
import org.apache.jackrabbit.ocm.mapper.Mapper;
import org.apache.jackrabbit.ocm.mapper.impl.annotation.AnnotationMapperImpl;
import org.apache.jackrabbit.ocm.query.Filter;
import org.apache.jackrabbit.ocm.query.Query;
import org.apache.jackrabbit.ocm.query.QueryManager;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.northpointdigital.demo.meetup.ocm.dao.EventDAO;
import com.northpointdigital.demo.meetup.ocm.model.Activity;
import com.northpointdigital.demo.meetup.ocm.model.Event;
import com.northpointdigital.demo.meetup.ocm.model.Location;

@Component
@Service
public class EventDAOImpl implements EventDAO {
    private static Logger log = LoggerFactory.getLogger(EventDAO.class);
    @Reference
    private SlingRepository repository;
    
    private Session session;
    private ObjectContentManager ocm;
    
    public void addOrUpdateEvent(Event event) throws RepositoryException {
        String path = event.getPath();
        if (session.nodeExists(path)) {
            ocm.update(event);
        } else {
            String parentPath = path.substring(0, path.lastIndexOf('/'));
            if (!session.nodeExists(parentPath)) {
                JcrUtil.createPath(parentPath, "nt:unstructured", session);
            }
            ocm.insert(event);
        }
        session.save();
    }

    public void removeEvent(Event event) throws RepositoryException {
        ocm.remove(event);
        session.save();
    }
    
    @Activate
    public void init() {
        // For demo ONLY. Do not use administrative in prod code
        try {
        session = repository.loginAdministrative(null); 
        
        List<Class> classes = new ArrayList<Class>();
        classes.add(Event.class);
        classes.add(Activity.class);
        classes.add(Location.class);
        Mapper mapper = new AnnotationMapperImpl(classes);
        ocm = new ObjectContentManagerImpl(session, mapper);
        } catch (RepositoryException e) {
            log.error("Cannot init: RepositoryException.");
        }
    }

    public Event getEvent(String path) throws RepositoryException {
        return (Event)ocm.getObject(Event.class, path);
    }
    
    public Event findEventByName(String name) throws RepositoryException {
        QueryManager queryManager = ocm.getQueryManager();

        Filter filter = queryManager.createFilter(Event.class);
        filter.addEqualTo("name", name);
        Query query = queryManager.createQuery(filter);
        return (Event) ocm.getObject(query);
    }
}
