package com.northpointdigital.demo.meetup.ocm.impl.beanconverter;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.ocm.exception.JcrMappingException;
import org.apache.jackrabbit.ocm.exception.ObjectContentManagerException;
import org.apache.jackrabbit.ocm.manager.atomictypeconverter.AtomicTypeConverterProvider;
import org.apache.jackrabbit.ocm.manager.beanconverter.impl.AbstractBeanConverterImpl;
import org.apache.jackrabbit.ocm.manager.objectconverter.ObjectConverter;
import org.apache.jackrabbit.ocm.mapper.Mapper;
import org.apache.jackrabbit.ocm.mapper.model.BeanDescriptor;
import org.apache.jackrabbit.ocm.mapper.model.ClassDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.northpointdigital.demo.meetup.ocm.model.HasPath;

/**
 * Map a bean attribute into a string JCR property that 
 * points to the actual node path.
 */
public class ReferencePathBeanConverterImpl extends AbstractBeanConverterImpl {
    private static final Logger log = LoggerFactory.getLogger(ReferencePathBeanConverterImpl.class);

    public ReferencePathBeanConverterImpl(Mapper mapper, ObjectConverter objectConverter, AtomicTypeConverterProvider atomicTypeConverterProvider)
    {
        super(mapper, objectConverter, atomicTypeConverterProvider);    
    }

    @Override
    public void insert(Session session, Node parentNode,
            BeanDescriptor beanDescriptor, ClassDescriptor beanClassDescriptor,
            Object object, ClassDescriptor parentClassDescriptor, Object parent)
            throws ObjectContentManagerException,
            org.apache.jackrabbit.ocm.exception.RepositoryException,
            JcrMappingException {
        insertOrUpdate(session, object);
    }

    @Override
    public void update(Session session, Node parentNode,
            BeanDescriptor beanDescriptor, ClassDescriptor beanClassDescriptor,
            Object object, ClassDescriptor parentClassDescriptor, Object parent)
            throws ObjectContentManagerException,
            org.apache.jackrabbit.ocm.exception.RepositoryException,
            JcrMappingException {
        insertOrUpdate(session, object);
    }
    
    private void insertOrUpdate(Session session, Object object) {
        String fullPath = "";
        try {
            fullPath = ((HasPath)object).getPath(); 
    
            int lastSlash = fullPath.lastIndexOf('/');
            String parentPath = fullPath.substring(0, lastSlash);
            String nodeName = fullPath.substring(lastSlash + 1);

            Node parentNode;
            if (session.nodeExists(parentPath)) {
                parentNode = session.getNode(parentPath);
            } else {
                parentNode = JcrUtil.createPath(parentPath, "nt:unstructured", session);
            }
            
            if (session.nodeExists(fullPath)) {
                objectConverter.update(session, parentNode, nodeName, object);
            } else {
                objectConverter.insert(session, parentNode, nodeName, object);
            }
        } catch (RepositoryException e) {
            log.error("Error while inserting/updating node using OCM: " + fullPath);
        }
    }
    
    @Override
    public void remove(Session session, Node parentNode,
            BeanDescriptor beanDescriptor, ClassDescriptor beanClassDescriptor,
            Object object, ClassDescriptor parentClassDescriptor, Object parent)
            throws ObjectContentManagerException,
            org.apache.jackrabbit.ocm.exception.RepositoryException,
            JcrMappingException {
        // Will not remove
    }

    @Override
    public Object getObject(Session session, Node parentNode, BeanDescriptor beanDescriptor, ClassDescriptor beanClassDescriptor, Class beanClass, Object parent) {
        String path;
        try {
            // Let's assume a convention here. ;)
            String property = beanDescriptor.getFieldName() + "Path";
            path = parentNode.getProperty(property).getString();
        } catch (RepositoryException e) {
            // Default get path
            path = getPath(session, beanDescriptor, parentNode);
        }
        return objectConverter.getObject(session, beanClass, path);
    }
}